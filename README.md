Testing / Assignment

About / Synopsis

This Project tells us how to execute Test Scripts from locally
Project status: working/prototype
Web Support

See real examples:

https://gitlab.com/irsatheng/fujitsuautomation/-/blob/master/README.md


Table of contents

Title / fujitsuautomation

About / Synopsis
Table of contents
Installation
[Pre-requisites]
[Test Script Execution]


**Installation:**

Download Git from the given url : https://git-scm.com/download/win

- [ ] Install the Git from the downloaded exe file.
- [ ] Create a folder from your local driver
- [ ] Open the folder and right click on anywhere inside the folder it shows you the option to open Git Bash Here. You need to open the Git Bash Command prompt window to get the latest code
- [ ] Now enter the command 'git clone https://gitlab.com/irsatheng/fujitsuautomation' to download the latest repository
- [ ] Go to the folder where you have created in step 3
- [ ] Verify there are 3 files gets downloaded inside the folder


Pre-requisites:

- [ ] Require Visual Studio
- [ ] Download Text Editor like Notepad++
- [ ] Nice to have Adobe PDFReader
- [ ] Install Visual Studio with added external libraries which supports NUnit Framework

(https://www.lambdatest.com/blog/setup-nunit-environment-with-visual-studio/)

**Test Script Execution:**

- [ ] Open Visual Studio
- [ ] Open UiTests.sln file from your local machine(Downloaded from git clone command)
- [ ] Build the solution
- [ ] Open Test Explorer 
- [ ] I have 2 Test files (ie)Test1 & Test2
- [ ] Select Test1 & run as selected tests(This test executes the scenario - TEST 1: HAPPY PATH, PURCHASE 2 ITEMS)
- [ ] Verify the results at the output tab 
- [ ] Select Test2 & run as selected tests(This test will execute the scenarios - TEST 2: REVIEW PREVIOUS ORDERS AND ADD A MESSAGE & TEST 3: CAPTURE IMAGES)
- [ ] I have encapsulated Test2 and Test3 in a single test called Test2
- [ ] At the end of the Test2 execution, You can see the screenshot in your Project Directory that is taken when the assertion gets failed


**API Execution:**

Pre-requisites

Install Postman application
Login as registered user

Execution Steps:
1. Open Postman application & Click Import option
2. Select the Collection Runner file from your local machine
3. Arrow icon on the Collection Runner file and select the Run button
4. Export Test Run at the end of the execution
5. Save Test run file in your local machine

  Note - Attached the Execution video for your reference

**Technical Debt:**

- [ ] In General - Automation does not cover up the frequent changes in application it might impact the stability of execution.With the number of browsers, devices and scripts growing with each test sprint, the release cycle would keep getting delayed leading to loss of time-to-market. Taking Execessive time for testing leads to business impact
- [ ] Lack of Knowledge 

- [ ] In Test Application: 
- [ ] Dynamically changing elements is a big challenge in automation. If the elements are changed day to day, Our Test Automation does not work with the dynamic attributes & resources. So handling the dynamic elements using different way of writing xpath is a good practice to avoid issues
- [ ] Continuous releases might impact the Automation 



**Improvements:**

- Prioritise & split up the Tasks
- Work with the Familiar Open Source Automation tool
- Schedule test periodically to verify the stability of an application
- Test Framework with CI/CD enabled service






